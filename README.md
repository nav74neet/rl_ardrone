# Reinforcement Learning for Autonomous navigation of UAVs
This repository contains the simulation source code for implementing reinforcement learning aglorithms for autonomous navigation of ardone in indoor environments. Gazebo is the simulated environment that is used here.
## Q-Learning.py
Autonomous Navigation of UAV using Q-Learning (Reinforcement Learning). 
- State: Discrete
- Action: Discrete
- Action space: 5x5 grid space.

Indoor Path Planning and Navigation of an Unmanned Aerial Vehicle (UAV) based on PID + Q-Learning algorithm (Reinforcement Learning). The quadrotor maneuvers towards the goal point, along the uniform grid distribution in the gazebo simulation environment(discrete action space) based on the specified reward policy, backed by the simple position based PID controller.

### Dependencies & Packages:
- Ubuntu 16.04 
- ROS Kinetic
- Gazebo 7
- ArDrone Autonomy ROS Package
- gym: 0.9.3
- TensorFLow 1.1.0 (preferrable with GPU support)
- Python: 2.7

### Reference:  
- Pham, Huy X., et al. Autonomous uav navigation using reinforcement learning. arXiv preprint arXiv:1801.05086 (2018).
- Mnih, Volodymyr, et al. Human-level control through deep reinforcement learning. Nature 518.7540 (2015)


## DDPG.py 
Deep Deterministic Policy Gradient algorithm is used for autonomous navigation of UAV from start to goal position. This is applicable for continuous action-space domain. (Under development!)

### Collaborator(s):
Arun Kumar (arunkumar12@iisc.ac.in) & Dr. S N Omkar (omkar@iisc.ac.in)
